import {
  Component,
  ChangeDetectionStrategy,
  NgModule,
  Input,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';

@Component({
  selector: 'app-lock',
  templateUrl: './lock.component.html',
  styleUrls: ['./lock.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    { provide: NG_VALUE_ACCESSOR, useExisting: LockComponent, multi: true },
  ],
})
export class LockComponent implements ControlValueAccessor {
  value!: boolean;
  isDisabled!: boolean;

  get icon(): 'lock' | 'lock_open' {
    return this.value ? 'lock' : 'lock_open';
  }

  onChange!: (value: boolean) => void;
  onTouched!: () => void;

  writeValue(obj: boolean): void {
    this.value = obj;
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(disabled: boolean): void {
    this.isDisabled = disabled;
  }

  setValue() {
    this.value = !this.value;
    this.onTouched();
    this.onChange(this.value);
  }
}

@NgModule({
  imports: [MatIconModule],
  declarations: [LockComponent],
  exports: [LockComponent],
})
export class LockModule {}
